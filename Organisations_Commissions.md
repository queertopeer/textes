# Commissions QueerToPeer

## Administratif
### Domaine de travail
La commission administratif s’occupe de tout ce qui touche à l’administratif : Iels remplissent les documents administratifs : Statuts d’association, Procès Verbal, Formulaires administratifs,…

### Membres
#### Attendu·es aux réunions
* Victor
* Kiky [Alexy]
* Mey
* Thenpan

## Community Management
### Domaine de travail
La commission community management s’occupe de tout ce qui touche à la communication : 
Elle s’occupe :
* Des réseaux sociaux, des mails, etc. 
* De représenter l’association à l’occasion d’interviews, de conférences, d’événementiels, etc.
* Des visuels (logo, bannières, flyers, etc.)

### Membres
#### Attendu·es aux réunions
* Powi
* Kiky [Alexy]
* Thenpan

## Finances
### Domaine de travail
La commission finances s’occupe de la trésorerie de l’association. Elle a le travail du Trésorier des associations traditionnelles.

### Membres
#### Attendu·es aux réunions
* Kiky [Alexy]
* Victor
* Powi

## Fonctionnement Interne
### Domaine de travail
La commission fonctionnement interne s’occupe du fonctionnement de l’association.
Elle gère l’organisation des commissions (dans les grosses lignes), de cette page, le fonctionnement du Discord,…

### Membres
#### Attendu·es aux réunions
* Victor
* Kiky [Alexy]
* Mey
* Thenpan

## Informatique
### Domaine de travail
La commission informatique s’occupe :
* Des serveurs de l’association (Administration Système).
* Du développement des logiciels, de l’envoie d’issues, du développement des sites web.
* Du développement du bot.
* Et tout ce qui est lié à l’informatique.

### Membres
#### Attendu·es aux réunions
* Victor
* Powi
* Louve Néon
* Shu

## Modération
### Domaine de travail
La commission Modération s’occupe de modérer les contenus, les commentaires, etc. Des services de QueerToPeer.

### Membres
#### Attendu·es aux réunions
* Mey
* Powi

#### Non attendu·es aux réunions
* Émile