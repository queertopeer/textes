Statuts de l’association QueerToPeer
====================================

Article 1 - Dénomination :
--------------------------

Il est fondé, entre les personnes adhérentes aux présents statuts, une association, régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour dénomination : « QueerToPeer ».  
Le nom de l'association pourra être raccourci en « Q2P ».

Article 2 - Objets et moyens d’action :
--------------------------------------

L'association se donne pour buts :

* de proposer une alternative aux GAFAM (Google, Apple, Facebook, Amazon, Microsoft) et autres géants d’internet. Q2P cherche à proposer aux utilisateurices queers (personnes Lesbiennes, Gays, Bi, Trans, Inter et autres personnes queers) des services libres, leur permettant de partager et de créer du contenu sans avoir à se soucier des censures queerphobes des grands groupes ;
* de défendre et protéger les droits des personnes queers ;
* d'agir pour la promotion d'un usage de l'informatique libre et éthique, basé sur des structures locales, notamment en participant à divers événements du libre et en encourageant la création de structures associatives de même nature.

Pour cela, l’association QueerToPeer, notamment, héberge des services libres à destination des personnes queers.

Article 3 - Siège Social :
--------------------------

Le siège social de l’association est situé à Lyon.  
Il pourra être transféré par simple décision du Conseil Collégial.

Article 4 - Durée
-----------------

La durée de l’association est illimitée.

Article 5 - Membres de l'Association
------------------------------------

L’association se compose de personnes adhérentes. Ces personnes seront uniquement des personnes physiques.  
Les membres de l'association sont les personnes physiques à jour dans leur adhésion ou dans le renouvellement de leur adhésion.  
L'adhésion est à renouveler tous les ans à la date anniversaire.


Article 6 - Admission
---------------------

Les demandes d'adhésion sont présentées au Conseil Collégial. Chaque membre prend l'engagement de respecter les présents statuts et le règlement intérieur qui lui sont communiqués à son entrée dans l'association.

Le Conseil Collégial pourra refuser des adhésions, avec avis motivé aux personnes intéressées dans la mesure du possible.

L'adhésion est gratuite, aucune cotisation annuelle n'est demandée aux personnes adhérentes.
Tous les membres peuvent voter et intervenir lors de l'Assemblée Générale.
Tous les membres peuvent interpeller directement le Conseil Collégial de l'association dès que nécessaire.

Article 7 - Radiations
----------------------

La qualité de membre se perd par :
1) La démission ;
2) Le décès ;
3) La radiation est prononcée par le Conseil Collégial ou par décision d'une Assemblée Générale lorsqu'une de ces assemblées estime que le membre est allé à l'encontre du règlement intérieur ou qu'iel a commis une faute grave.
Le membre intéressé pourra se justifier devant l'assemblée ayant décidé de sa sanction avant que celle-ci ne confirme sa décision.

Article 8 - Affiliations
------------------------

La présente association n'est affiliée à aucune fédération. Elle peut néanmoins adhérer à d’autres associations, unions ou regroupements par décision du Conseil Collégial.

Article 9 - Ressources
----------------------

Les ressources de l'association comprennent :

1) Les donations ponctuelles de personnes physiques ou morales ;
2) Les subventions de l'Etat, des régions, des départements et des communautés de communes ;
3) Les indemnités délivrées par des personnes physiques ou morales, souhaitant faire réaliser un bien ou un service par l'association, dans le cadre d'un partenariat ou d'un événement solidaire ;
4) Toutes les ressources autorisées par les lois et règlements en vigueur.

Article 10 - Assemblée Générale Ordinaire
-----------------------------------------

L'Assemblée Générale Ordinaire comprend tous les membres de l'association.  
Elle se réunit au minimum une fois par an.  
L'Assemblée Générale Ordinaire ne peut délibérer valablement que si le tiers plus un des membres est présent ou représenté.

7 jours au moins avant la date fixée, les membres de l'association sont convoqués par les soins du Conseil Collégial. L'ordre du jour figure sur les convocations.  
Le Conseil Collégial préside l'assemblée et expose la situation morale de l'association.  
Le Conseil Collégial rend compte de la gestion de l'association à l'Assemblée Générale Ordinaire.  
Le Conseil Collégial soumet les comptes annuels (bilan, compte de résultat et annexe) à l'approbation de l'assemblée.  
L’Assemblée Générale Ordinaire discute de la composition des membres et des conditions d'adhésion.
Les autres points abordés doivent être inscrits à l'ordre du jour.  
Il est procédé, après épuisement de l'ordre du jour, au renouvellement des membres sortants du conseil.  
Les décisions sont prises à la majorité des suffrages exprimés par les membres présents ou représentés.  
Les décisions des assemblées générales s’imposent à tous les membres, y compris absents ou représentés.  
Le Conseil Collégial communique le compte-rendu de chaque assemblée générale à l'ensemble des membres de l'association.  

Article 11 - Assemblée Générale Extraordinaire
----------------------------------------------

À son initiative ou suite à une demande écrite d'au moins 25% des membres, le Conseil Collégial convoque une Assemblée Générale Extraordinaire.  
L'Assemblée Générale Extraordinaire ne peut délibérer valablement que si le tiers plus un des membres est présent ou représenté.  
Ne peuvent y être abordées que la modification des statuts ou la dissolution de l'association.  
Les modalités de convocation sont les mêmes que pour l’Assemblée Générale Ordinaire.  
La modification des statuts sera approuvée à la majorité absolue, la dissolution de l'association sera prononcée si elle réunie au moins les 3/4 des membres.  

Article 12 - Le Conseil Collégial
---------------------------------

L'association est gérée de manière consultative par un conseil de minimum 7 membres, élus pour une année par l'Assemblée Générale Ordinaire. Ses membres sont rééligibles.

Le Conseil Collégial se réunit au moins une fois tous les 2 mois, plus si nécessaire, sur demande d'au moins un de ses membres.  
Les décisions sont prises à la majorité absolue. En cas de partage, les membres du Conseil Collégial votent de nouveau.  
En cas de non disponibilité, le conseil remplace provisoirement les membres du Conseil Collégial concernés par des membres volontaires, et ce jusqu'à la fin de la réunion.

Tout membre du Conseil Collégial qui, sans justification, n'aura pas assisté à 3 réunions consécutives sera considéré comme démissionnaire.
Si la situation l'exige, le Conseil Collégial peut déléguer en partie ses pouvoirs, pour une durée déterminée, à un ou plusieurs des autres membres volontaires de l'association.
Le Conseil Collégial devra dans ce cas tenir au courant l'intégralité de ses membres.
Les pouvoirs des membres ainsi élus prennent fin à l'expiration du mandat des membres remplacés.

Le Conseil Collégial pourra choisir de déléguer une ou plusieurs de ses tâches à un ou des groupes de personnes de son choix pour une durée déterminée ou non.

L'association sera représentée administrativement par des membres de ce conseil, sur la base du volontariat, avec une nombre minimum de 3 personnes représentantes de l'association.

Si un membre représentant administratif de l'association ne fait plus partie du conseil ou de l'association, il ne pourra plus assurer cette tâche.
Dans le cas où le nombre de personne représentantes de l'association deviendrait inférieur à 3, le Conseil Colléial devra ajouter un ou des nouveaux membres aux représentants  pour qu'ils reste en nombre suffisant.
Cette nouvelle personne représentante sera choisie parmi les membres du Conseil Collégial.

Article 13 - Exclusion du Conseil Collégial
-------------------------------------------

Tout membre du Conseil Collégial qui fait l'objet d'une mesure d'exclusion de l'association sera remplacé-e conformément aux dispositions de l'article 12 des présents statuts.

Article 14 - Modalités de vote
------------------------------
Les votes pour les différents besoins listés ci-dessus pourront être réalisés par voie électronique à partir du moment où on peut identifier les personnes votantes comme adhérentes de l'association.  
Le décompte des voix pour une majorité absolue se fait parmi les personnes présentes et représentées.

Article 15 - Indemnités
-----------------------

Toutes les fonctions, y compris celles des membres du Conseil Collégial, sont gratuites et bénévoles.  
Seuls les frais occasionnés par l’accomplissement du mandat sont remboursés sur présentation de justificatifs.  
Le rapport financier présenté à l’Assemblée Générale Ordinaire rend compte des remboursements, par bénéficiaire.

Article 16 - Règlement Intérieur
--------------------------------

Un règlement est établi par le Conseil Collégial, qui le fait alors approuver par l'Assemblée Générale Ordinaire.

Ce règlement éventuel est destiné à fixer les divers points non prévus par les présents statuts, notamment ceux qui ont trait à l'administration interne de l'association.

Ce règlement intérieur peut recevoir des suggestions de n'importe quel membre, ces suggestions seront ensuite soumises au vote de l'ensemble des membres de l'association.

Après approbation par le Conseil Collégial, le règlement intérieur s'impose à tous les membres de l'association.

Article 17 - Dissolution
------------------------

En cas de dissolution prononcée selon les modalités prévues à l’article 11, un ou plusieurs liquidateurs sont nommés. Et l'actif net, s'il y a lieu, est dévolu à un organisme ayant un but non lucratif, ou à une association ayant des objectifs similaires à QueerToPeer, conformément aux décisions de l’Assemblée Générale Extraordinaire qui statue sur la dissolution. L’actif net ne peut être dévolu à un membre de l’association, même partiellement, sauf reprise d’un apport.

Fait à Lyon, le 20 février 2019.

Signature des membres du Conseil Collégial représentants de l'association :

* Powi Ewyn DENIS
* Alexy Maryan Jeannine Michele CATELLE
* Julien PHILIPPON
* Meyssam KLEIN
* Victor DOUCET