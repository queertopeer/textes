## Organisation :

Les **documents importants** tels que les statuts sont mis à la [racine du projet](/).

Les **comptes rendus** vont dans le dossier [comptes rendus](/Comptes%20Rendus).

## Documents importants :

* [Statuts](/Statuts.md)