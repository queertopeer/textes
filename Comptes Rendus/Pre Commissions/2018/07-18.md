Compte-rendu réunion 18/07/18
=============================

Présent·e·s :  Alexy, Powi, Mey, Nono, Klev, Natir, Ov  
CR : Alexy  
Facilitation : Powi

* Dates butoires pour CGU & Positions
* Rappel du besoin de rapidité
* Prochaine réunion thématique ?
* Fixer une réunion pour les statuts
* Quid de la deuxième partie de la réunion ? Relecture des CGUs ?
  * Powi propose date butoire le Mercredi 25 Juillet 23h59-59 pour les CGU's et positions puis écriture d'une vidéo de présentation pour tourner et publier la video le dimanche 29. Avoir le minimum pour publier des vidéos.


https://queertube.org/accounts/ljul/videos quid de la chaine ?

Demander dans les CGU aux personnes de mettre des Trigger Warning. Dans la description, la vidéo, le titre, la miniature ?

CGU's retirer le cadre legal. remplacer par tout "dans le respect des lois en vigeur dans votre pays"

attendre avoir vu avec le centre LGBTI Lyon pour diffuser l'annonce de siege social.

relecture des CGU's.

Powi : les personnes problematiques font peur/fuir les personnes trans, besoin d'un espace propre.
Les personnes trans vont se casser, la personne pas safe (TERF) va rester, et c'est pas ce qu'on veut.

sous quelles conditions on accepte les associations et entreprises ?

CGU version majeure relue.

Reste a faire :
* Faire valider les CGU
* Convertir les CGU's pour queertube voir vérification orthographique avec Ovehli
* Corriger et traduire la CGU
* Reste a voir CGU :
  * personnes morales ?
  * placement de produits