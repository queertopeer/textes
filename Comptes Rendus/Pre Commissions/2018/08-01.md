Compte-rendu réu 01/08/18
=========================

Présent.e.s: Alexy, Klev, mey, TIBB, Vitria, Powi, Ovehli  
CR: TIBB  
Facilitation: TIBB

Les CGUs :
* Les retours :
  * Notes, vide. [Fait]
  * Renseigner sur le droit d'auteur dedans.
  * Pas possible de live (ben oui lol)
  * Nous (?), Vous (?), QueerTube (?); au niveau légal à définir https://cdn.discordapp.com/attachments/463405165190447114/474297471338545153/unknown.png
  * (autres sujets)  https://media.discordapp.net/attachments/463405165190447114/474297269705768960/unknown.png?width=314&height=676 (INCLUS/PAS INCLUS)
  * ""Je trouve que l'utilisation des TW/CW que vous voulez faire amènera forcément à des dérives. Notamment déjà le fait de mettre TW/CW dans le titre qui a quelque chose de particulièrement angoissant, le fait que le sexisme soit pas mentionné, mais également les TW d'animaux. Le problème, c'est que genre, dès que tu mentionneras un animal, faudrait mettre un CW alors que, dans les faits, y'a moins de gens qui ont peur d'un lapin que d'une araignée (notamment à cause des constructions sociales). Et surtout, c'est une utilisation des TW/CW qui devient angoissante très très vite, amène a une espèce de safitude pas toujours très bien selon moi (quand je parle de safitude c'est pas dans le sens "faire de son mieux" mais "si tu te foires tu peux avoir la communauté entière qui te tombe dessus") et ça aura tendance à rebuter pas mal de personnes. Pour moi c'est de l'utilisation TW/CW avec les dérives twitter qui vont avec, ou les militantes afro-fem se font tomber sur la gueule pour un truc et harceler pendant une semaine, où les militant-es anti-arabophobie aussi.  TLDR : Je pense que cette utilisation des CW/TW est à revoir, notamment pour le côté angoissant du truc, genre, mettre un CW dans le titre c'est bien beau mais perso les chiens j'trouve ça mignon, les violences policières ça me fait rien par contre tu parles de violences faites aux femmes, oui.""
  * → https://pad.powi.fr/p/CGU_1.1
* A Modifier:
  * Dans un document connexe rappeler ce qu'implique être un "simple" hébergeur <3


Les tâches:
* Dates butoires
  * mercredi 8 août CGU
  * Lundi 6 Août Candidtures CM
  * Mercredi 8 Août Faire tests (et rapports rapides?), pour savoir au niveau des questions techniques sur la qualité et la place.

* Community management :
  * Qu'est ce que l'on peut poster ? CALENDRIER DE DATES IMPORTANTES ANYWAY Vote "compte vivant" / "compte carré"
  * Compte vivant? Méthodologie autour de ce compte vivant répondre couramment, tours de garde courts (maxi une semaine), signaler qui est derrière le compte au début du tour de garde, réagir actu twitter, chercher les embrouilles?, ne pas se laissser embrouiller gratos?, jeux débiles?, c'est la tchatche, partage nus militants queer, Relayer les concernés
Compte carré? Méthodo de ce compte. Tour de garde plus long possibles (2 semaines), signaler qui est derrière mais pas nécessairement mais possible, autour de l'actu (si volonté) faire des communiqués, ne pas se laisser embrouiller MAIS réponse sérieuse et flegmatiques, partage nu militant queer, partage nus militants queer, relayer les concernés

/!\ Alexy empli de pertinence: "Si tu sens que ça chauffe pour toi, tu passes le compte twitter aux copains, te laisse pas submerger! è_é"

/!\ APPEL À candidature pour volontaiures du twitwi & mastodon :3 

* Qu'est-ce que l'on évite de poster ? DU CUL, de l'illégal et/ou contre les règles de la plate-forme, ne pas boire de la 8.6 avant de poster, "si tu le poste pas sur ton compte, le poste pas là, tu poste au nom de tous"

Vegan Extrêmiste crypto-salamiste "J'aimerai que le compte queertube soit bonne ambiance tout en transmettant les bonnes infos. Si on peut faire des communiqués pour les journées internationales, ça serait génial !"
* Communiqué pour les journées internationales concernant les queers ? Les femmes ? Les racisé.es ? OUI les queer, OUI les femmes, OUI Racisés. (On fait partir l'info des CONCERNES dedieu, c'est élémentaire chouchou)
* Si communiqué : mettons en avant des assos concerné.es ?  ben ui

Anglais 
* Quand est-ce qu’on s’y met ? ^w^ (à angliciser le truc)  
--> Need V1 CGU (donc tâches sur la CGU), mais la traduire (déhà ouechouechtavu)  
--> Sur le CM: Sur les communiqués, sur les grands rappels des dates, sur les grandes annonces (si on tchatche pas besoin d'être bilingue)

Autres
* Transcodage ? (480p/toussa) Si transcode Peertube garde les originaux.  
  Consommation de 30 à 50% en plus si plusieurs qualités disponibles -> Test sur un premier temps? ! 
Mettre sur une base de 480p?
Tests et questionnements: Fluctuations de place au transcodage, limitation de qualité max à 720p? (est-ce nécessaire d'avoir une qualité de ouf), tester différence de qualité, tester différence de place en fonction des vidéos

* Newsletter/communiqué chaque mois faire du RSS? Genre, un fichier pdf à poster sur Twitter. Intégration Mastodon/twitwi possible  
  Oui, newslester avec un bouton unsuscribe évident histoire de pas faire nos gros reloux. Renseigner sur nouveautés Peertube/Queertube. Vote NL/RSS sur les nouveautés? Ou sur la régularité?    
   
   /!\ Pas hésiter à recruter des gens qui pourraient être intéresser didiou è_é

Liste Techos: Powi, Louve-Néon (??), Vitria

Pause en milieu de réunion où on fait les cakes, sinon, c'est chaud! Boire, pas que de la 8.6 (voire l'alcool c'est crade pour la santé)


ON RAMENE LES COPAINES! (nanaïs, )