## Organisation

Les conventions de nommage et de localisation pour les comptes-rendus sont les suivantes : `/Compte Rendus/<Nom de la commission>/<année>/<mois>-<jour>.md` (le numéro du mois sera préfixé d'un zéro si il ne contient qu'un seul chiffre)

## Comptes rendus par commission

* [Administratif](Administratif)
   * [2019](Administratif/2019)
* [Finances](Finances)
    * [2019](Finances/2019)
* [Fonctionnement interne](Fonctionnement Interne)
    * [2019](Fonctionnement Interne/2019)
* [Informatique](Informatique)
    * [2019](Informatique/2019)
* [Avant la présence des commissions](Pre Commissions)